<?php

namespace app\models;

use Yii;
use kartik\password\StrengthValidator;

/**
 * This is the model class for table "tbl_user".
 *
 * @property string $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $role_id
 * @property string $password_hash
 *
 * @property TblRole $role
 */
class TblUser extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $authKey;
    //public $accessToken;
    public $password;
    public $captcha;
    private $sendVerification = false;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'password'], 'required', 'on' => 'register'],
            [['role_id', 'email_verified'], 'integer'],
            [['name', 'phone', 'password_hash'], 'string', 'max' => 60],
            [['name', 'phone'], 'string', 'min' => 4],
            [['password'], StrengthValidator::className(), 'min' => 6, 'lower' => 1, 'upper' => 0, 'digit' => 1, 'special' => 0, 'userAttribute' => 'name', 'on' => 'register'],
            [['email'], 'string', 'max' => 256],
            [['email'], 'unique'],
            [['email'], 'email', 'message' => 'Значение должно быть в формате E-mail'],
            [['captcha'], 'captcha', 'on' => 'register'],
            [['email_verified'], 'safe', 'on' => 'verifymail'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'E-mail',
            'role_id' => 'ID роли',
            'password_hash' => 'Хеш пароля',
            'password' => 'Пароль',
            'email_verified' => 'Подтвержден ли E-mail',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(TblRole::className(), ['id' => 'role_id']);
    }
    
    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::find()->where(['email' => $email])->one();
    }
    
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }
    
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->password_hash = static::createPasswordHash($this->password);
                $this->authKey = \Yii::$app->security->generateRandomString();
                $this->role_id = TblRole::ROLE_USER;
                
                $this->sendVerification = true;
            }
            return true;
        }
        return false;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        
        if ($this->sendVerification) {
            //$code = Yii::$app->security->generateRandomString();
            $code = TblEmailVerification::generateCode();
            $verification = new TblEmailVerification([
               'userId' => $this->id, 
               'code' => $code,
            ]);
            $verification->save();
            
            $verificationLink = \yii\helpers\Url::to(['/site/verifymail', 'uid' => $this->id, 'code' => $code], true);
            Yii::$app->mailer->compose()
                ->setFrom('system.testapp@yandex.ru')
                ->setTo($this->email)
                ->setSubject('Подтверждение регистрации в системе (test_app)')
                ->setHtmlBody('Для завершения регистрации перейдите по ссылке ' . $verificationLink)
                ->send();
            
            $this->sendVerification = false;
        }
    }
    
    public function getIsAdmin()
    {
        return ((int) $this->role_id === TblRole::ROLE_ADMIN) ? true : false;
    }
    
    public static function createPasswordHash($password)
    {
        return Yii::$app->getSecurity()->generatePasswordHash($password);
    }
}
