<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => Yii::$app->user->isGuest ? 'test_task' : ' Привет, ' . \yii\helpers\Html::encode(Yii::$app->user->identity->name) . '!',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
$navBarExtraOptions = [];
if (Yii::$app->user->isGuest) {
    $navBarExtraOptions[] = ['label' => 'Войти', 'url' => ['/site/login']];
} else {
    if (Yii::$app->user->identity->isAdmin) {
        $navBarExtraOptions[] = [
            'label' => 'Админка',
            'url' => ['/tbl-user/index'],
            'linkOptions' => ['data-method' => 'post']
        ];
    }
    
    $navBarExtraOptions[] = [
        'label' => 'Выйти из системы',
        'url' => ['/site/logout'],
        'linkOptions' => ['data-method' => 'post']
    ];
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => array_merge([
        ['label' => 'Главная', 'url' => ['/site/index']],
        ['label' => 'О приложении', 'url' => ['/site/about']],
    ], $navBarExtraOptions),
]);
NavBar::end();
?>