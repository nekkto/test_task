<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\TblRole;
use app\models\TblUser;
use app\components\AccessRule;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'only' => ['logout', 'admin'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['admin'],
                        'allow' => true,
                        'roles' => [
                            TblRole::ROLE_ADMIN
                        ],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionAdmin()
    {
        return $this->render('admin');
    }
    
    public function actionRegister()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new \app\models\TblUser(['scenario' => 'register']);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', 'Новый пользователь добавлен. Для завершения регистрации следует подтвердить E-mail, пройдя по ссылке из письма. Письмо отправлено на ' . $model->email);
            return $this->goHome();
        } else {
            return $this->render('register', ['model' => $model]);
        }
    }
    
    public function actionVerifymail($uid, $code)
    {
        $model = \app\models\TblEmailVerification::find()
                ->where(['userId' => $uid, 'code' => $code])
                ->one();
        $user = \app\models\TblUser::findOne($uid);
        if ($model) {
            $user->email_verified = 1;
            $user->save();
            \Yii::$app->user->login($user, 0);
            \Yii::$app->getSession()->setFlash('success', 'Е-mail успешно подтвержден.');
            $model->delete();
        } elseif (!$user) {
            \Yii::$app->getSession()->setFlash('error', 'Пользователь с заданным идентификатором не найден.');
        } else {
            \Yii::$app->getSession()->setFlash('error', 'Код подтверждения неверен или уже был использован ранее.');
        }
        
        return $this->goHome();
    }
}
